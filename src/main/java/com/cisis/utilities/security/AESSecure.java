/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cisis.utilities.security;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author joel.eze
 */
public class AESSecure {
    private static final String ALGO = "AES";

    public static String encrypt(String data, String webServiceKey) throws Exception {
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, generateKey(webServiceKey));
        byte[] encVal = c.doFinal(data.getBytes());
        return new String(Base64.encodeBase64(encVal));
    }

     public static String decrypt(String encryptedData, String webServiceKey) throws Exception {
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, generateKey(webServiceKey));
        byte[] decordedValue = Base64.decodeBase64(encryptedData);
        byte[] decValue = c.doFinal(decordedValue);
        return new String(decValue);
    }
     /**
     * Generate a new encryption key.
     */
    private static Key generateKey(String key) throws Exception {
        return new SecretKeySpec(key.getBytes(), ALGO);
    }
}
